#Morpion 22.04.2021 - 255 lignes avec commentaires -

# A ajouter aux jeux
# - IA

# axe X : 275 250 275
# axe Y : 275 250 275
import pygame
from time import sleep

pygame.init()

screen = pygame.display.set_mode((800, 800))
pygame.display.set_caption('Tik Tak éclaté')
font_name = pygame.font.match_font('arial')

limite1 = 275
limite2 = 525

regulCross = 95

posLineX = 0
posLineY = 0

case1 = 165
case2 = 405
case3 = 650

caseTaken = 9 * [0]

scoreCross = 0
scoreCircle = 0

fRondCroix = 1          # 1 = croix 2 = rond
run = 1
win = 0
draw = 0
modif = 1
mode = 0                #mode 1 = 2 joeurs mode 2 = IA

clock = pygame.time.Clock()
screen.fill((255, 255, 255))

################################ FONCTIONS #############################################################################

def interface():
    axeX = 275
    axeY = 50
    longueur = 700
    espace = 250
    epaisseur = 4
    #ligne horizontal
    pygame.draw.line(screen, (0, 0, 0), (axeX, axeY), (axeX, axeY + longueur), epaisseur)
    pygame.draw.line(screen, (0, 0, 0), (axeX + espace, axeY), (axeX + espace, axeY + longueur), epaisseur)

    axeX = 50
    axeY = 275
    #ligne vertical
    pygame.draw.line(screen, (0, 0, 0), (axeX, axeY), (axeX + longueur, axeY), epaisseur)
    pygame.draw.line(screen, (0, 0, 0), (axeX, axeY + espace), (axeX + longueur, axeY + espace), epaisseur)

def drawCircle(posClicX, posClicY):
    # Axe X : case 1 : 165      case 2 : 400        case 3 : 650
    # Axe Y :
    # case 1 : 165
    # case 4 : 400
    # case 7 : 650
    pygame.draw.circle(screen, (0, 0, 0), (posClicX, posClicY), 80, 11)

def drawCross(posClicX, posClicY):
    # Axe X : case 1 : 70     case 2 : 305    case 3 : 555
    # Axe Y :
    # case 1 : 70
    # case 4 : 305
    # case 7 : 555
    longueur = 175
    pygame.draw.line(screen, (0, 0, 0), (posClicX, posClicY), (posClicX + longueur, posClicY + longueur), 15)
    pygame.draw.line(screen, (0, 0, 0), (posClicX, posClicY + longueur), (posClicX + longueur, posClicY), 15)

def draw_text(surf, text, size, x, y):
    font = pygame.font.Font(font_name, size)
    text_surface = font.render(text, True, 'Black')
    text_rect = text_surface.get_rect()
    text_rect.midtop = (x, y)
    surf.blit(text_surface, text_rect)

def scoreUpdate(scoreCross, scoreCircle):
    draw_text(screen, str(scoreCross), 40, 375, 15)
    draw_text(screen, '-', 40, 405, 15)
    draw_text(screen, str(scoreCircle), 40, 435, 15)

############################### MAIN ###################################################################################

interface()
scoreUpdate(scoreCross, scoreCircle)

while run:

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            x, y = event.pos
            if(x < limite1):
                x = case1
                if (y < limite1 and caseTaken[0] == 0):
                    y = case1           #case 1 = 165
                    caseTaken[0] = fRondCroix
                elif (y < limite2 and y > limite1 and caseTaken[3] == 0):
                    y = case2           #case 4 = 405
                    caseTaken[3] = fRondCroix
                elif (y > limite2 and caseTaken[6] == 0):
                    y = case3           #case 7 = 650
                    caseTaken[6] = fRondCroix
            elif(x < limite2):
                x = case2
                if (y < limite1 and caseTaken[1] == 0):
                    y = case1           #case 2 = 165
                    caseTaken[1] = fRondCroix
                elif (y < limite2 and y > limite1 and caseTaken[4] == 0):
                    y = case2           #case 5 = 405
                    caseTaken[4] = fRondCroix
                elif (y > limite2 and caseTaken[7] == 0):
                    y = case3           #case 8 = 650
                    caseTaken[7] = fRondCroix
            else:
                x = case3
                if (y < limite1 and caseTaken[2] == 0):
                    y = case1           #case 3 = 165
                    caseTaken[2] = fRondCroix
                elif (y < limite2 and y > limite1 and caseTaken[5] == 0):
                    y = case2           #case 6 = 405
                    caseTaken[5] = fRondCroix
                elif (y > limite2 and caseTaken[8] == 0):
                    y = case3           #case 9 = 650
                    caseTaken[8] = fRondCroix

            if(y == case1 or y == case2 or y == case3):
                modif = 1

            #Changement de joeur
            if(modif):
                if (fRondCroix == 2):
                    drawCircle(x, y)
                    fRondCroix = 1
                else:
                    drawCross(x - regulCross, y - regulCross)
                    fRondCroix = 2
                y = 0
                modif = 0
                draw += 1

            #AJOUTER la ligne de victoire

            #condition de victoire
            #1er Ligne
            if(caseTaken[0] == 1 and caseTaken[1] == 1 and caseTaken[2] == 1 or caseTaken[0] == 2 and caseTaken[1] == 2 and caseTaken[2] == 2):
                if (caseTaken[0] == 1):
                    scoreCross += 1
                else:
                    scoreCircle += 1
                win = 1
                posLineX = 50
                posLineY = 162.5
                pygame.draw.line(screen, (0, 0, 0), (posLineX, posLineY), (posLineX + 700, posLineY), 30)
            #Ligne diagonale en haut à gauche
            elif(caseTaken[0] == 1 and caseTaken[4] == 1 and caseTaken[8] == 1 or caseTaken[0] == 2 and caseTaken[4] == 2 and caseTaken[8] == 2):
                if (caseTaken[0] == 1):
                    scoreCross += 1
                else:
                    scoreCircle += 1
                win = 1
                posLineX = 50
                posLineY = 50
                pygame.draw.line(screen, (0, 0, 0), (posLineX, posLineY), (posLineX + 700, posLineY + 700), 40)
            #1er Colonne
            elif(caseTaken[0] == 1 and caseTaken[3] == 1 and caseTaken[6] == 1 or caseTaken[0] == 2 and caseTaken[3] == 2 and caseTaken[6] == 2):
                if (caseTaken[0] == 1):
                    scoreCross += 1
                else:
                    scoreCircle += 1
                win = 1
                posLineX = 162.5
                posLineY = 50
                pygame.draw.line(screen, (0, 0, 0), (posLineX, posLineY), (posLineX, posLineY + 700), 30)
            #2ème ligne
            elif(caseTaken[3] == 1 and caseTaken[4] == 1 and caseTaken[5] == 1 or caseTaken[3] == 2 and caseTaken[4] == 2 and caseTaken[5] == 2):
                if (caseTaken[3] == 1):
                    scoreCross += 1
                else:
                    scoreCircle += 1
                win = 1
                posLineX = 50
                posLineY = 400
                pygame.draw.line(screen, (0, 0, 0), (posLineX, posLineY), (posLineX + 700, posLineY), 30)
            #3ème ligne
            elif(caseTaken[6] == 1 and caseTaken[7] == 1 and caseTaken[8] == 1 or caseTaken[6] == 2 and caseTaken[7] == 2 and caseTaken[8] == 2):
                if (caseTaken[6] == 1):
                    scoreCross += 1
                else:
                    scoreCircle += 1
                win = 1
                posLineX = 50
                posLineY = 650
                pygame.draw.line(screen, (0, 0, 0), (posLineX, posLineY), (posLineX + 700, posLineY), 30)
            #2ème Colonne
            elif(caseTaken[1] == 1 and caseTaken[4] == 1 and caseTaken[7] == 1 or caseTaken[1] == 2 and caseTaken[4] == 2 and caseTaken[7] == 2):
                if (caseTaken[1] == 1):
                    scoreCross += 1
                else:
                    scoreCircle += 1
                win = 1
                posLineX = 400
                posLineY = 50
                pygame.draw.line(screen, (0, 0, 0), (posLineX, posLineY), (posLineX, posLineY + 700), 30)
            #3ème Colonne
            elif(caseTaken[2] == 1 and caseTaken[5] == 1 and caseTaken[8] == 1 or caseTaken[2] == 2 and caseTaken[5] == 2 and caseTaken[8] == 2):
                if (caseTaken[2] == 1):
                    scoreCross += 1
                else:
                    scoreCircle += 1
                win = 1
                posLineX = 650
                posLineY = 50
                pygame.draw.line(screen, (0, 0, 0), (posLineX, posLineY), (posLineX, posLineY + 700), 30)
            #Ligne diagonale en haut à droite
            elif(caseTaken[2] == 1 and caseTaken[4] == 1 and caseTaken[6] == 1 or caseTaken[2] == 2 and caseTaken[4] == 2 and caseTaken[6] == 2):
                if(caseTaken[2] == 1):
                    scoreCross += 1
                else:
                    scoreCircle += 1
                win = 1
                posLineX = 50
                posLineY = 750
                pygame.draw.line(screen, (0, 0, 0), (posLineX, posLineY), (posLineX + 700, posLineY - 700), 40)

            if(win or draw == 9):
                pygame.display.update()
                sleep(1)
                screen.fill((255, 255, 255))
                if win:
                    draw_text(screen, 'WIN', 225, 400, 220)
                else:
                    draw_text(screen, 'DRAW', 225, 400, 225)
                pygame.display.update()
                sleep(2)
                screen.fill((255, 255, 255))
                interface()
                scoreUpdate(scoreCross, scoreCircle)
                win = 0
                draw = 0
                posLineX = 0
                posLineY = 0
                for i in range(9):
                    caseTaken[i] = 0

    pygame.display.update()

pygame.quit()